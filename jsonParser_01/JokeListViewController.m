//
//  JokeListViewController.m
//  jsonParser_01
//
//  Created by Pudel_dev on 18/06/15.
//  Copyright (c) 2015 Pudel. All rights reserved.
//

#import "JokeListViewController.h"
#import "NSString+HTML.h"

@interface JokeListViewController ()

@end

@implementation JokeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadJson];
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor purpleColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self action:@selector(reLoadJson) forControlEvents:UIControlEventValueChanged];
    //test
    
}
-(void)loadJson
{
    NSError * error = nil;
    NSData * jokesJson = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://api.icndb.com/jokes/random/10"]];
    id jokesObjects = [NSJSONSerialization JSONObjectWithData:jokesJson options:NSJSONReadingMutableContainers error:&error];
    NSArray * jokesList = [jokesObjects objectForKey:@"value"];
    
    _jokes = [[NSMutableArray alloc]init];
    
    for(NSDictionary * jokeItem in jokesList)
    {
        [_jokes addObject:[jokeItem[@"joke"]stringByConvertingHTMLToPlainText]];
    }
}
-(void)reLoadJson
{
    [self loadJson];
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];

}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _jokes.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"JokeCell";
    JokeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    int row = [indexPath row];
    cell.jokeCellLabel.text = _jokes[row];
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
