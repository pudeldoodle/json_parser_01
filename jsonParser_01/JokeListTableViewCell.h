//
//  JokeListTableViewCell.h
//  jsonParser_01
//
//  Created by Pudel_dev on 18/06/15.
//  Copyright (c) 2015 Pudel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JokeListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *jokeCellLabel;


@end
