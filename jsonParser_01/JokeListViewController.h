//
//  JokeListViewController.h
//  jsonParser_01
//
//  Created by Pudel_dev on 18/06/15.
//  Copyright (c) 2015 Pudel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JokeListTableViewCell.h"

@interface JokeListViewController : UITableViewController

@property (nonatomic,strong) NSMutableArray * jokes;
@property (nonatomic,strong) UIRefreshControl * refreshControl;

@end
